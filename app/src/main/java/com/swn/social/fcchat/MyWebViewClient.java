package com.swn.social.fcchat;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Message;
import android.util.Log;
import android.view.Window;
import android.webkit.HttpAuthHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class MyWebViewClient extends WebViewClient{
	
	private Thread tDlg;
	private static final String URI_FC_TLD = "fotochatter.com";
	
	/* (non-Javadoc)
	 * @see android.webkit.WebViewClient#onFormResubmission(android.webkit.WebView, android.os.Message, android.os.Message)
	 */
	@Override
	public void onFormResubmission(WebView view, Message dontResend,
			Message resend) {
		
		
		if(FCChat.DBG_MODE)
    		Log.i("onFormResubmission","called: "+resend);
		
		view.goBack();
	}

    @Override public boolean shouldOverrideUrlLoading(WebView view, String url){

    	if(FCChat.DBG_MODE)
    		Log.i("KamaWebViewClient","shouldOverrideUrlLoading called");

    	String tmpHost = FCChat.ctx.getString(R.string.http_host);
    	
		  if (url.startsWith("http://"+tmpHost, 0)) 
		  {
		  	view.loadUrl(url);
		  	return true;
		  }
		  else if(url.contains(URI_FC_TLD)){
			  if(FCChat.DBG_MODE)
			  		Log.i("wvc","passing fc url to external browser");
			  String tmpUrl = url + "?h=" + ChatActivity.getLoginHash();
			  
				if(FCChat.DBG_MODE)
			  		Log.i("wvc","loading url: "+tmpUrl);
				
				Intent inSd = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
				ChatActivity.getCtx().startActivity(inSd);
				
				return true;
		  }
		  else 
		  {
			 if(FCChat.DBG_MODE)
		  		Log.i("wvc","passing unknown url to external browser");
			 
			Intent inSd = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			ChatActivity.getCtx().startActivity(inSd);
			
		    return true;
		  }

    }
    
    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
    	
    	if(FCChat.DBG_MODE)
			Log.i("onPageStarted","called: "+url);
    	
    	//showParentProgressDialog("Working..", "Loading Data");
    	if(!ChatActivity.isQuietRefresh())
    	{
	    	ChatActivity.getHandler().post(new Runnable() {
				public void run() {
					ChatActivity.openDlg("Working..", "Loading Data");
				}
			});
    	
	    	tDlg = new Thread(new Runnable(){
	    		public void run() {
	    			try{
	    				Thread.sleep(14000);
	    			} catch(InterruptedException e){
	    				if(FCChat.DBG_MODE)
	    					Log.w("dlgThread","Dlg Watch thread terminating..");
	    				return;
	    			}
	    			if(FCChat.DBG_MODE)
						Log.w("dlgThread","closing dialogs");
	    			
	    			ChatActivity.getHandler().post(new Runnable(){
	    	    		public void run() {
	    	    			ChatActivity.closeDlg();
	    	    		}
	    	    	});
	    		}
	    	});
	    	tDlg.start();
    	}
    	else{
    		ChatActivity.getHandler().post(new Runnable() {
				public void run() {
					((Activity)ChatActivity.getCtx()).setProgressBarIndeterminateVisibility(true);
				}
			});
    	}
    	
    	super.onPageStarted(view, url, favicon);
    }
    
    @Override
    public void onPageFinished(WebView view, String url) {
    	if(FCChat.DBG_MODE)
    		Log.i("KamaWebViewClient","onPageFinished called");
    	
    	ChatActivity.closeDlg();
    	
    	try{
    		if(tDlg.isAlive())
    			tDlg.interrupt();
    	}catch(Exception e){
    	}
    	
    	if(ChatActivity.isQuietRefresh()){
    		ChatActivity.setQuietRefresh(false);
    		ChatActivity.getHandler().post(new Runnable() {
				public void run() {
					((Activity)ChatActivity.getCtx()).setProgressBarIndeterminateVisibility(false);
				}
			});
    	}
    	
    	super.onPageFinished(view, url);
    }
    
    @Override
    public void onReceivedError(WebView view, int errorCode,
    		String description, String failingUrl)
    {
    	if(FCChat.DBG_MODE)
    		Log.i(getClass().getName(),"recieved wv error: "+description);
    	
    	ChatActivity.closeDlg();
    	
    	Thread t = new Thread(new Runnable(){
    		public void run() {
    			ChatActivity.getHandler().post(new Runnable() {
                    public void run() {
                    	Toast.makeText(ChatActivity.getCtx(),"A Network error has occured, please wait 5 seconds or reload app.",
        						7000).show();
                    }
                });
    		}
    	});
    	
    	t.start();
    	
    	try{
    		Thread.sleep(5000);
    		view.loadUrl(failingUrl);
    		return;
    	} catch(InterruptedException e){}
    	
    	
    	t.destroy();
    	
    	super.onReceivedError(view, errorCode, description, failingUrl);
    }
    
    @Override
    public void onReceivedHttpAuthRequest(WebView view,
    		HttpAuthHandler handler, String host, String realm) {
    	ChatActivity.closeDlg();
    	super.onReceivedHttpAuthRequest(view, handler, host, realm);
    }
    
    @Override
    public void onTooManyRedirects(WebView view, Message cancelMsg,
    		Message continueMsg) {
    	ChatActivity.closeDlg();
    	super.onTooManyRedirects(view, cancelMsg, continueMsg);
    }

}
