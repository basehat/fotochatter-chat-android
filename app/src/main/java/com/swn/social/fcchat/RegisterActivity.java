package com.swn.social.fcchat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class RegisterActivity extends Activity {

	private Context ctx;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		
		setContentView(R.layout.register);
		
		ctx = this;
		
		Button bRegister = (Button)findViewById(R.id.do_register2);
		bRegister.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				EditText etUser = (EditText)findViewById(R.id.reg_name);
				EditText etPass = (EditText)findViewById(R.id.reg_pass);
				EditText etPass2 = (EditText)findViewById(R.id.reg_pass2);
				EditText etEmail = (EditText)findViewById(R.id.reg_email);
				
				RadioButton cbMale = (RadioButton)findViewById(R.id.radio_male);
				RadioButton cbFemale = (RadioButton)findViewById(R.id.radio_female);
				
				String gender = null;
				
				if(etUser.length()<=4){
					Toast.makeText(ctx, "Please enter a username.", Toast.LENGTH_LONG).show();
					return;
				}
				
				if(etEmail.length()==0 || !etEmail.getText().toString().contains("@")){
					Toast.makeText(ctx, "Please enter an email address.", Toast.LENGTH_LONG).show();
					return;
				}
				
				if(!etPass.getText().toString().contentEquals(etPass2.getText().toString())){
					Log.e("register button","pass1: "+etPass.getText().toString()+", 2: "+etPass2.getText().toString());
					Toast.makeText(ctx, "Passwords do not match.", Toast.LENGTH_LONG).show();
					return;
				}
				
				if(cbMale.isChecked())
					gender = "M";
				else if(cbFemale.isChecked())
					gender = "F";
				
				if(gender==null){
					Toast.makeText(ctx, "Please select your gender.", Toast.LENGTH_LONG).show();
					return;
				}
				
				Integer ret = new Integer(FcAdapter.checkUsername(ctx, etUser.getText().toString()));
				FCChat.log("got return: "+ret);
				if(ret==0){
					Toast.makeText(ctx, "Username not available, please try again...", Toast.LENGTH_LONG).show();
					return;
				}
				
				Toast.makeText(ctx, "Registering user..", Toast.LENGTH_LONG).show();
				
				String ret2 = FcAdapter.register(ctx, etUser.getText().toString(), 
											etPass.getText().toString(), 
											etEmail.getText().toString(),
											gender);
				if(ret2=="0"){
					Toast.makeText(ctx, "Error registering account, please try again.", Toast.LENGTH_LONG).show();
					return;
				}
				
				Toast.makeText(ctx, "User registered, logging in.", Toast.LENGTH_SHORT).show();
				
				FCChat.log("saving user logindata: "+ret2);
    			SharedPreferences.Editor editor = FCChat.userConfig.edit();
	    		editor.putString(FCChat.PREF_KEY_LOGINDATA,ret2);
	    		editor.putString(FCChat.PREF_KEY_LOGINUSER,etUser.getText().toString());
	    		editor.putString(FCChat.PREF_KEY_LOGINPASS,etPass.getText().toString());
	    		editor.commit();
				
				Intent in = new Intent(ctx,ChatActivity.class);
				in.putExtra(FCChat.PREF_KEY_LOGINDATA, FCChat.userConfig.getString(FCChat.PREF_KEY_LOGINDATA, null));
				startActivity(in);
			}
		});
		
	}

	
	
}
