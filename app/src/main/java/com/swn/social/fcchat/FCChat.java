package com.swn.social.fcchat;

import java.util.AbstractCollection;
import java.util.Iterator;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class FCChat extends Activity {
	
	public static final boolean DBG_MODE = false;
	public static final String PREF_FILE = "config";
	public static final String PREF_KEY_LOGINDATA = "login_data";
	public static final String PREF_KEY_LOGINUSER = "login_user";
	public static final String PREF_KEY_LOGINPASS = "login_pass";
	
	public static final String DELIM = "||";
	
	public static SharedPreferences userConfig = null;
	public static ProgressDialog pd;
	public static Handler handler = new Handler();
	public static Context ctx = null;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        ctx = this;
        userConfig = getSharedPreferences(PREF_FILE, 0);
        
        EditText etUser = (EditText)findViewById(R.id.login_username);
        EditText etPass = (EditText)findViewById(R.id.login_password);
        
        String tmpUser = userConfig.getString(PREF_KEY_LOGINUSER, null);
        if(tmpUser!=null)
        {
        	log("setting user hint to : "+tmpUser);
        	etUser.setText(tmpUser);
            etPass.setText(userConfig.getString(PREF_KEY_LOGINPASS, null));
        }
        
        Button bRegister = (Button) findViewById(R.id.do_register);
        bRegister.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				Intent in = new Intent(ctx,RegisterActivity.class);
				startActivity(in);
			}
		});
        
        Button bLogin = (Button) findViewById(R.id.do_login);
        bLogin.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				pd = ProgressDialog.show(ctx, "Logging in", "Please wait..", true,false);
				new Thread(new Runnable() {
					public void run() {
						if(doLogin()){
							Intent in = new Intent(ctx,ChatActivity.class);
							in.putExtra(PREF_KEY_LOGINDATA, userConfig.getString(PREF_KEY_LOGINDATA, null));
							startActivity(in);
						} else{
						}
					}
				}).start();
				return;
			}
		});
    }
    
    public static void log(String m){
    	log("FCChat",m,0);
    }
    
    public static void log(String m,int mode){
    	log("FCChat",m,mode);
    }
    
    public static void log(String s,String m,int mode){
    	if(DBG_MODE){
    		switch(mode){
    		default:
    		case(0):
    			Log.i(s,m);
    			break;
    		case(1):
    			Log.w(s,m);
    			break;
    		case(3):
    			Log.e(s,m);
    			break;
    		}
    	}
    }
    
    private void closeDlg(){
    	try{
    		pd.dismiss();
    	} catch(Exception e){
    	}
    }
    
    private void sendToast(final String m){
    	handler.post(new Runnable() {
			
			public void run() {
				Toast.makeText(ctx, m, Toast.LENGTH_LONG).show();
			}
		});
    }
    
    private boolean doLogin()
    {
    	EditText etUser = (EditText) findViewById(R.id.login_username);
    	EditText etPass = (EditText) findViewById(R.id.login_password);
    	
    	if(etUser.getText().length()<=3){
    		closeDlg();
    		sendToast("Please enter a username");
    		return false;
    	}
    	
    	if(etPass.getText().length()==0){
    		closeDlg();
    		sendToast("Please enter a password");
    		return false;
    	}
    	//Toast.makeText(this, "Username: "+etUser.getText(), Toast.LENGTH_LONG).show();
    	
    	String ret = FcAdapter.login(this, etUser.getText().toString(), etPass.getText().toString());
    	
    	closeDlg();
    	
    	if(ret.contains(DELIM)){
    		sendToast("Thank you for logging in!");
    		CheckBox rem = (CheckBox)findViewById(R.id.remember_me);
    		if(rem.isChecked()){
    			log("saving user logindata");
    			SharedPreferences.Editor editor = userConfig.edit();
	    		editor.putString(PREF_KEY_LOGINDATA,ret);
	    		editor.putString(PREF_KEY_LOGINUSER,etUser.getText().toString());
	    		editor.putString(PREF_KEY_LOGINPASS,etPass.getText().toString());
	    		editor.commit();
	    		return true;
    		} else
    		{
    			//TODO: decide if we want to forget settings..
    			return true;
    		}
    	}else{
    		sendToast("Login failed, please try again.");
    	}
    	
    	return false;
    }
    
    public static String join( String[] strings, String token )
    {
        StringBuffer sb = new StringBuffer();
       
        for( int x = 0; x < ( strings.length - 1 ); x++ )
        {
            sb.append( strings[x] );
            sb.append( token );
        }
        sb.append( strings[ strings.length - 1 ] );
       
        return( sb.toString() );
    }
    
    public boolean onMenuOpened(int featureId, Menu menu) {
		
		FCChat.log("fcchat menu opened");
		
		return super.onMenuOpened(featureId, menu);
	}
}