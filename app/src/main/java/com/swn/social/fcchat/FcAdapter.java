package com.swn.social.fcchat;

import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import android.content.Context;

public class FcAdapter {

	public static final String ACTION_LOGIN = "login";
	public static final String ACTION_POST = "post";
	public static final String ACTION_CHECKUSER = "checkuser";
	public static final String ACTION_REGISTER = "register";
	
	public static String login(Context ctx, String username,String password){
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("username", username);
		params.put("password", password);
		
		
		String ret = FcAdapter.doRequest(ctx, ACTION_LOGIN, params,false,"");
		
		FCChat.log("login - response: "+ret);
		
		return ret;
	}
	
	public static String postMessage(Context ctx, Integer chatid, String message, String loginhash){
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("message",message);
		params.put("chatid",chatid.toString());
		
		String ret = FcAdapter.doRequest(ctx, ACTION_POST, params,true,loginhash);
		
		return ret;
	}
	
	public static String checkUsername(Context ctx, String username){
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("username",username);
		
		String ret = FcAdapter.doRequest(ctx, ACTION_CHECKUSER, params,false,"");
		
		return ret;
	}
	
	public static String register(Context ctx, String username, String password, String email, String sex){
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("username",username);
		params.put("password",password);
		params.put("email",email);
		params.put("sex",sex);
		
		String ret = FcAdapter.doRequest(ctx, ACTION_REGISTER, params,true,"");
		
		return ret;
	}
	
	public static String doRequest(Context ctx, String action, HashMap<String, String> hmap, boolean post, CharSequence loginhash){
		
		FCChat.log("fcadapter - generating request");
		
		String tmpUrl = "http://" +ctx.getString(R.string.http_host) + "/" +
						ctx.getString(R.string.http_controller) + "/" +
						action + "/";
		String tmpData[] = new String[hmap.size()];
		String postData = null;
		
		if(hmap!=null)
		{
			Set<Map.Entry<String, String>> s = hmap.entrySet();
			
			if(!post)
			{
				for(Map.Entry<String, String> o : s){
					tmpUrl += o.getKey() + "/" + o.getValue() + "/";
		 		}
			}
			else
			{
				int x =0;
				for(Map.Entry<String, String> o : s){
					try{
					tmpData[x] = URLEncoder.encode(o.getKey(), "UTF-8") + 
									"=" + URLEncoder.encode(o.getValue(), "UTF-8");
					} catch(Exception e){
						FCChat.log("error adding post value: "+e);
					}
					x++;
		 		}
				postData = FCChat.join(tmpData, "&");
				FCChat.log("post data: "+postData);
			}
		}
		
		if(loginhash.length()>0){
			try{
			tmpUrl += "?h="+loginhash.toString();
			}catch (Exception e) {
				// TODO: handle exception
			}
		}
		
		FCChat.log("fcadapter - using url: "+tmpUrl);
		
		try{
			URL u = new URL(tmpUrl);
	        HttpURLConnection c = (HttpURLConnection) u.openConnection();
	        
	        if(post){
	        	c.setRequestMethod("POST");
	        }
	        else{
	        	c.setRequestMethod("GET");
	        }
	        
	        c.setDoOutput(true);
	        c.connect();
	        
	        if(post){
		        OutputStreamWriter wr = new OutputStreamWriter(c.getOutputStream());
		        wr.write(postData);
		        wr.flush();
	        }
	        
	        InputStream in = c.getInputStream();
	        byte[] buffer = new byte[1];

	        String ret = new String();
	        while((in.read(buffer,0,1))!=-1){
	        	ret += new String(buffer);
	        }
	        c.disconnect();
	        in.close();
	        FCChat.log("fcadapter - return size: "+ret.length());
	        return ret;
		} catch(Exception e){
			FCChat.log("fcadapter error: "+e);
			return null;
		}
	}
	
}
