package com.swn.social.fcchat;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.PendingIntent.CanceledException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.Window;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

public class ChatActivity extends Activity {

	public static ProgressDialog pd;
	
	private static final int MenuGroupMain = 1;
	private static final int MenuGroupContext = 2;
	private static final int MenuGroupContext2 = 3;
	
	private static final int MenuItem1 = Menu.FIRST + 1;
	private static final int MenuItem2 = Menu.FIRST + 2;
	private static final int MenuItem3 = Menu.FIRST + 3;
	private static final int MenuItem4 = Menu.FIRST + 4;
	private static final int MenuItem5 = Menu.FIRST + 5;
	private static final int dlgRefreshDelay = 1;
	
	private static final int ACTION_WEB_CHATLIST = -1;
	private static final int ACTION_WEB_QUICKPROFILE = -2;
	
	private static  Handler handler;
	private static Context ctx;
	
	private Thread tRefresh = null;
	
	private static WebView wv = null;
	private String tmpHost;
	private String tmpController;
	private static String loginHash;
	private Integer chatId = 0;
	private String chatString = null;
	private Integer refreshRate = 10;
	
	private static final String PREF_KEY_CHATREFRESH = "chat_refresh";
	
	public static final CharSequence[] slideShowDelay = {"10", "20", "30", "60"};

	private static final int ACTIVITY_RESULT_SENDINTENT = 0;
	
	private static boolean quietRefresh = false;
	
	class JsAdapter
	{
		public void setChatData(int id,String name)
		{
			if(true)
			{
				FCChat.log("setting chatid to: "+id);
				chatId = id; 
				chatString = name;
				
				handler.post(new Runnable() {
					public void run() {
						LinearLayout e = (LinearLayout)findViewById(R.id.message_entry_con);
						if(chatId==0){
							setTitle("Fotochatter Chatroom List");
							e.setVisibility(View.GONE);
							try{
							tRefresh.interrupt();
							tRefresh.join();
							}catch(Exception e1){
							}
						}
						else if(chatId<0){
							switch(chatId){
							case(ACTION_WEB_QUICKPROFILE):
								setTitle("Fotochatter Profile");
								break;
							case(ACTION_WEB_CHATLIST):
								setTitle("Fotochatter Chatroom Users");
								break;
							}
							
							e.setVisibility(View.GONE);
							try{
								tRefresh.interrupt();
								tRefresh.join();
								}catch(Exception e2){
								}
						}
						else{
							setTitle("Fotochatter Chatroom - "+chatString);
							e.setVisibility(View.VISIBLE);
							
							try{
							tRefresh.interrupt();
							tRefresh.join();
							} catch(Exception e2){
								
							}
							
							tRefresh = new Thread(new Runnable() {
								public void run() {
									try{
										while(true){
											Thread.sleep(refreshRate*1000);
											FCChat.log("refreshing chat");
											setQuietRefresh(true);
											wv.reload();
											}
									} catch(Exception e3){
										FCChat.log("exiting watch thread: "+e3);
									}
								}
							});
							
							try{
								tRefresh.start();
							} catch(Exception e3){
							}
						}
					}
				});
			} 
			else
				FCChat.log("ignoring setchatdata call");
		}
	}
	
	public static String ucfirst(String in){
		String first = in.substring(0, 1);
		return in.toLowerCase().replaceFirst(first, first.toUpperCase());
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		FCChat.log("chatactivity - oncreate");
		
		super.onCreate(savedInstanceState);
		setTheme(android.R.style.Theme_Light_NoTitleBar);
		
		int i = getWindow().getWindowManager().getDefaultDisplay().getOrientation();
		
		if(i
					!=1)
		{
			FCChat.log("chatactivity - setting requested orientation");
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			return;
		}
		
		
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		
		setContentView(R.layout.chat);
		
		setProgressBarIndeterminateVisibility(false);
		
		refreshRate = FCChat.userConfig.getInt(PREF_KEY_CHATREFRESH, 10);
		
		/*LinearLayout panel = (LinearLayout)findViewById(R.id.RelativeLayout01);
		
		Button button1 = new Button(ctx);
		button1.setLayoutParams(new LinearLayout.LayoutParams
		(LinearLayout.LayoutParams.WRAP_CONTENT,

		LinearLayout.LayoutParams.WRAP_CONTENT));
		button1.setText(" Done ");
		button1.setGravity(Gravity.CENTER);
		button1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
			}
		});
		button1.setPadding(3,3,6,3);
		button1.setClickable(false);
		button1.setEnabled(false);
		
		panel.addView(button1);*/
		
		//LinearLayout lt = (LinearLayout)findViewById(R.id.RelativeLayout01);
		//lt.setOrientation(LinearLayout.VERTICAL);
		
		tmpHost = FCChat.ctx.getString(R.string.http_host);
		tmpController = FCChat.ctx.getString(R.string.http_controller); 
		setCtx(this);
		setHandler(new Handler());
		
		try{
		    Bundle extras = getIntent().getExtras();
	        if (extras != null) {
	        	
	        	wv = (WebView) findViewById(R.id.webview);
	    		wv.getSettings().setJavaScriptEnabled(true);
	    		wv.getSettings().setSupportZoom(true);
	    		wv.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
	    		wv.getSettings().setPluginsEnabled(false);
	    		wv.getSettings().setSaveFormData(false);
	    		wv.getSettings().setSavePassword(false);
	    		wv.getSettings().setBuiltInZoomControls(false);
	    		wv.getSettings().setLightTouchEnabled(true);
	    		//wv.setScrollContainer(false);
	    		
	    		wv.addJavascriptInterface(new JsAdapter(),"kamadapter");
	    		wv.setWebViewClient(new MyWebViewClient());
	        	
	            String[] logindata = extras.getString(FCChat.PREF_KEY_LOGINDATA).split("\\|\\|");
	            String tmpUrl = "http://"+tmpHost+"/"+tmpController+"/index";
	            
	            tmpUrl += "?h="+logindata[2];
	            setLoginHash(logindata[2]);
	            
	            FCChat.log("fetching android index page with url: "+tmpUrl);
	            
	            Button bSend = (Button)findViewById(R.id.send_message);
	    		bSend.setOnClickListener(new OnClickListener() {
	    			public void onClick(View v) {
	    				EditText e = (EditText)findViewById(R.id.message_entry);
	    				String message = e.getText().toString();
	    				if(message.length()==0){
	    					Toast.makeText(ctx, "Please enter a message.", Toast.LENGTH_SHORT).show();
	    				} else {
	    					tRefresh.interrupt();
	    					e.setText("");
	    					Toast.makeText(ctx, "Posting Message..", Toast.LENGTH_LONG).show();
	    					String ret = FcAdapter.postMessage(ctx, chatId, message, getLoginHash());
	    					FCChat.log("got return: "+ret);
	    					setQuietRefresh(true);
	    					wv.reload();
	    				}
	    			}
	    		});
	    		setQuietRefresh(true);
	            wv.loadUrl(tmpUrl);
	        }
	    } catch(Exception e){
	    	FCChat.log("error prearing wv url from extras: "+e);
	    	this.finish();
	    }
	    
	}

	
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
	    	
	    	FCChat.log("got back button");
	    	
	    	if(wv.canGoBack())
	    	{
	    		FCChat.log("going back");
	    		 
	    		wv.goBack();
	    		
	    		return true;
	    	} else {
	    		handler.post(new Runnable() {
					public void run() {
						finish();
					}
				});
	    	}
	    }
	    return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		prepareMenu(menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		prepareMenu(menu);
		return super.onPrepareOptionsMenu(menu);
	}
	
	private void prepareMenu(Menu menu)
	{
		menu.clear();
		menu.setQwertyMode(true);
		
		switch(chatId)
		{
		default:
			MenuItem item2 = menu.add(MenuGroupMain, MenuItem2, 1,"User List");
			{
				item2.setTitleCondensed("Users");
				item2.setAlphabeticShortcut('w');
				item2.setIcon(android.R.drawable.ic_menu_info_details);
				item2.setOnMenuItemClickListener(new OnMenuItemClickListener() {
					public boolean onMenuItemClick(MenuItem item) {
						String tmpUrl;
						try {
							tmpUrl = "http://"+tmpHost+"/"+tmpController+"/userlist/chatroom/"+URLEncoder.encode(chatString, "UTF-8");
							tmpUrl += "?h="+getLoginHash();
							wv.loadUrl(tmpUrl);
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							//e.printStackTrace();
						}
						return false;
					}
				});
			}
			
			MenuItem item3 = menu.add(MenuGroupMain, MenuItem3, 2,"Leave Room");
			{
				item3.setAlphabeticShortcut('l');
				item3.setTitleCondensed("Leave");
				item3.setIcon(android.R.drawable.ic_menu_close_clear_cancel);
				item3.setOnMenuItemClickListener(new OnMenuItemClickListener() {
					public boolean onMenuItemClick(MenuItem item) {
						String tmpUrl = "http://"+tmpHost+"/"+tmpController+"/index";
			            tmpUrl += "?h="+getLoginHash();
						wv.loadUrl(tmpUrl);
						return true;
					}
				});
			}
			
			SubMenu item5 = menu.addSubMenu(MenuGroupMain, MenuItem5, 3,"Options");
			{
				//item5.setAlphabeticShortcut('o');
				item5.add(MenuGroupContext,MenuItem1,1,"Refresh Rate: "+refreshRate+"s");
				item5.setIcon(android.R.drawable.ic_menu_preferences);
			}
		case(-1):
			MenuItem item4 = menu.add(MenuGroupMain, MenuItem4, 0,"Invite Friend");
			{
				//item4.setAlphabeticShortcut('s');
				//item4.add(MenuGroupContext2,MenuItem1,1,"Contacts/Email");
				//item4.add(MenuGroupContext2,MenuItem2,2,"FC User");
				item4.setIcon(android.R.drawable.ic_menu_send);
			}
		case(0):
			MenuItem item1 = menu.add(MenuGroupMain, MenuItem1, 4,"Reload");
			{
				item1.setAlphabeticShortcut('r');
				item1.setIcon(android.R.drawable.ic_menu_compass);
			}
			if(chatId==0){
				MenuItem item7 = menu.add(MenuGroupMain, MenuItem2, 4,"Logout");
				{
					item7.setAlphabeticShortcut('q');
					item7.setIcon(android.R.drawable.ic_menu_delete);
					item7.setOnMenuItemClickListener(new OnMenuItemClickListener() {
						public boolean onMenuItemClick(MenuItem item) {
							
							finish();
							
							return true;
						}
					});
				}
			}
			break;
		case(-2):
			MenuItem item8 = menu.add(MenuGroupMain, MenuItem1, 4,"Reload");
			{
				item8.setAlphabeticShortcut('r');
				item8.setIcon(android.R.drawable.ic_menu_compass);
			}
			MenuItem item7 = menu.add(MenuGroupMain, MenuItem1, 4,"Back to Chatroom");
			{
				item7.setAlphabeticShortcut('b');
				item7.setIcon(android.R.drawable.ic_media_previous);
				item7.setOnMenuItemClickListener(new OnMenuItemClickListener() {
					public boolean onMenuItemClick(MenuItem item) {
						String tmpUrl;
						try {
							tmpUrl = "http://"+tmpHost+"/"+tmpController+"/chat/chatroom/"+URLEncoder.encode(chatString, "UTF-8");
							tmpUrl += "?h="+getLoginHash();
							FCChat.log("reenter chatroom url: "+tmpUrl);
							wv.loadUrl(tmpUrl);
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							//e.printStackTrace();
						}
						return true;
					}
				});
			}
			break;
		}
	}
	

	
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		
		FCChat.log("menu item selected");
		
		switch(item.getGroupId())
		{
		case(MenuGroupContext):
			switch(item.getItemId())
			{
			case(MenuItem1):
				((Activity) ctx).showDialog(dlgRefreshDelay);
				break;
			}
			break;
		case(MenuGroupMain):
			switch(item.getItemId())
			{
			//reload button
			case(MenuItem1):
				FCChat.log("reloading webkit");
			getHandler().post(new Runnable() {
					public void run() {
						setQuietRefresh(true);
						wv.reload();
					}
				});
			
				break;
			case(MenuItem4):
				String tmpName = "newbies";
			
				try {
					tmpName = URLEncoder.encode(chatString, "UTF-8");
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				String msgBody = "Come join my fotochatter chatroom '"+chatString+"', you can use your phone,pc,laptop,or android/iphone!\n";
				msgBody += "\nWEB: http://www.fotochatter.com/chatroom/"+tmpName+" - Join chat on the website\n";
				msgBody += "\nMobile: http://wap.fotochatter.com/chatroom/"+tmpName+" - Join chat on the mobile site\n";
				msgBody += "\niPhone: http://wap.fotochatter.com/chatroom/"+tmpName+" - Join chat on the mobile site";
				msgBody += "\nAndroid: market://search?q=fotochatter - Download FC Chat for Android\n";
				
				Intent sendIntent = new Intent(Intent.ACTION_SEND);
				sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Come join my fc chatroom");
				sendIntent.putExtra(Intent.EXTRA_TEXT, msgBody);
				sendIntent.setType("text/plain");
				 //sendIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:/"+tmpFilename));
				 //Intent in = Intent.createChooser(sendIntent, "Send with");
				// PendingIntent p = PendingIntent.getActivity(ctx, ACTIVITY_RESULT_SENDINTENT, in, Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(Intent.createChooser(sendIntent, "Send with:")); //$NON-NLS-1$ 
				 try {
					//p.send(ACTIVITY_RESULT_SENDINTENT);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
				break;
			}
			break;
		}
		
		return super.onMenuItemSelected(featureId, item);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		
		Dialog dialog;
		
		switch(id){
		case(dlgRefreshDelay):
			
			tRefresh.interrupt();
			
			AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
			builder.setTitle("Change Delay");
			builder.setItems(slideShowDelay, new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog, int item) {
			    	
			    	try{
			    		SharedPreferences.Editor editor = FCChat.userConfig.edit();
			    		editor.putInt(PREF_KEY_CHATREFRESH,Integer.parseInt(slideShowDelay[item].toString()));
			    		editor.commit();
			    	} catch(Exception e){
			    		FCChat.log("error commiting pref changes: "+e);
			    	}
			    	setQuietRefresh(true);
			        wv.reload();
			        Toast.makeText(ctx, "Changed delay to "+slideShowDelay[item], Toast.LENGTH_SHORT).show();
			    }
			});
			
			AlertDialog alert = builder.create();
			
			alert.setOnDismissListener(new DialogInterface.OnDismissListener(){
				public void onDismiss(DialogInterface dialog) {
					setQuietRefresh(true);
					wv.reload();
				}
			});
			
			dialog = alert;
			
			break;
		default:
			dialog = null;
			break;
		}
		
		return dialog;
	}

	public static void setHandler(Handler handler) {
		ChatActivity.handler = handler;
	}



	public static Handler getHandler() {
		return handler;
	}
	
	
	public static void openDlg(final String title,final String message)
	{
		handler.post(new Runnable() {
			public void run() {
				ChatActivity.pd = ProgressDialog.show(getCtx(), title, message, true,false);
			}
		});
	}
	
	public static void closeDlg()
	{
		try{
    		pd.dismiss();
    	} catch(Exception e){
    	}
	}



	public static void setCtx(Context ctx) {
		ChatActivity.ctx = ctx;
	}



	public static Context getCtx() {
		return ctx;
	}



	public void setChatId(Integer chatId) {
		this.chatId = chatId;
	}



	public Integer getChatId() {
		return chatId;
	}

	public static void setQuietRefresh(boolean qrefresh) {
		quietRefresh = qrefresh;
	}

	public static boolean isQuietRefresh() {
		return quietRefresh;
	}

	public void setLoginHash(String loginHash) {
		this.loginHash = loginHash;
	}

	public static String getLoginHash() {
		return loginHash;
	}

	
	
}
